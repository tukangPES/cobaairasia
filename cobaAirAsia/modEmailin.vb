﻿Option Strict On

Imports System.Net.Mail


Public Module modGeneral
    Public Function SendEmail( _
            ByVal iEmailaddr As String, _
            ByVal iSubject As String, _
            ByVal ibodymessage As String, _
            ByVal iFullpath As String, _
            Optional ByVal iIsHTML As Boolean = False) As Boolean

        Dim avalue As Boolean = False
        Dim aMailMsg As MailMessage = Nothing
        Dim aMailAttachment As Attachment = Nothing
        Dim aError As String = String.Empty
        Dim aListEmail() As String = Strings.Split(iEmailaddr, ";")
        Dim aidx As Integer
        Dim ClientAddress As New MailAddress("safe@ultrajaya.co.id")

        Try
            aMailMsg = New MailMessage

            For aidx = 0 To aListEmail.Length - 1
                aMailMsg.To.Add(aListEmail(aidx))
            Next
            aMailMsg.From = ClientAddress
            aMailMsg.Subject = iSubject


            aMailMsg.Body = ibodymessage
            aMailMsg.IsBodyHtml = iIsHTML
            If iFullpath <> "" Then
                aMailAttachment = New Attachment(iFullpath)
                aMailMsg.Attachments.Add(aMailAttachment)
            End If

            Dim aClient As SmtpClient = New SmtpClient("mail.ultrajaya.co.id")
            Dim aCred As New Net.NetworkCredential("safe@ultrajaya.co.id", "ultra2006")

            'Add credentials if the SMTP server requires them
            aClient.Credentials = aCred
            aClient.Send(aMailMsg)
            aMailMsg.Dispose()
            avalue = True
        Catch ex As Exception
            MsgBox("error send email : " & ex.Message)
        End Try

        Return avalue
    End Function
End Module

